1. `git clone`
2. `cd stage-2-express-yourself-with-nodejs`
3. `npm i`
4. `npm start`
5. By default server running on [localhost:3000](http://localhost:3000)

<blockquote>
  <details>
    <summary>🇺🇦-translation</summary>

### Опис

API для масиву бійців, яких ми можемо добавляти, змінювати, удаляти та зчитувати. Також можемо отримати кількість перемог бійців та збільшити їх якщо це необхідно.Цей невеличкий сервер створений для роботи сумісно з Fighter Game.

#### Запити

- **GET**: _/user_  
  отримання масиву всіх користувачів

### Опис

- **GET**: _/user/:id_  
  отримання одного користувача по ID

- **POST**: _/user_  
  створення користувача за даними з тіла запиту

- **PUT**: _/user/:id_  
  оновлення користувача за даними з тіла запиту

- **DELETE**: _/user/:id_  
  видалення одного користувача по ID

- **GET**: _/victories_  
  отримання масиву з імен та кількості перемог

- **PUT**: _/victories/:id_  
  записати одну перемогу для користувача по ID

  </details>

  <details>
    <summary>🇷🇺-translation</summary>

### Описание

    API для массива бойцов, которых мы можем добавлять, изменять, удалят и считывать. Также можем получить количество побед бойцов и увеличить их если это необходимо.Цей небольшой сервер создан для работы совместно с Fighter Game.

#### Запросы

- **GET**: _/user_  
  получение массива всех пользователей

- **GET**: _/user/:id_  
  получение одного пользователя по ID

- **POST**: _/user_  
  создание пользователя по данным передаваемым в теле запроса

- **PUT**: _/user/:id_  
  обновление пользователя по данным передаваемым в теле запроса

- **DELETE**: _/user/:id_  
  удаление одного пользователя по ID

- **GET**: _/victories_  
  получения массива который состоит с имен и количества побед

- **PUT**: _/victories/:id_  
  записать одну победу для пользователя по ID

    </details>
  </blockquote>

### Description

The API for the array of fighters we can add, modify, delete, and read. We can also get the number of troop victories and increase them if necessary. This small server is designed to work in conjunction with Fighter Game.

### Main Requests

- **GET**: _/user_  
  get an array of all users

- **GET**: _/user/:id_  
  get one user by ID

- **POST**: _/user_  
  create user according to the data from the request body

- **PUT**: _/user/:id_  
  update user according to the data from the request body

- **DELETE**: _/user/:id_  
  delete one user by ID

- **GET**: _/victories_  
  get an array that consists of names and number of wins

- **PUT**: _/victories/:id_  
  change the number of user wins by ID
