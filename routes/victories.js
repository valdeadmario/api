const express = require("express");
const router = express.Router();

const users = require("../store/store");
/**
 * This function give out count of victories.
 * @param {string} url /users
 * @returns {Array} array of users.
 */
router.get("/", function(req, res, next) {
  const victoriesOfUsers = users.map(item => ({
    _id: item._id,
    name: item.name,
    victories: item.victories
  }));
  if (req && victoriesOfUsers) {
    res.send(victoriesOfUsers);
  }
});
/**
 * This function give out concrete users victories.
 * @param {number} id id of user
 * @returns {Object} count of victories by ID.
 */
router.put("/:id", function(req, res, next) {
  const id = req.params.id;
  if (id) {
    const idx = users.findIndex(item => item._id === id);
    if (idx !== -1) {
      let victories = +users[idx].victories + 1;
      const newFighter = { _id: id, ...users[idx], victories };
      users.splice(idx, 1, newFighter);
      res.send(newFighter);
    } else {
      res.status(404).send("User not found");
    }
  } else {
    res.status(404).send("User not found");
  }
});

module.exports = router;
