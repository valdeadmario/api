const express = require("express");
const fs = require("fs");
const router = express.Router();

const { saveName } = require("../services/user.service");
const { isAuthorized } = require("../middlewares/auth.middleware");

const users = require("../store/store");
/**
 * This function give out const users.
 * @param {string} url /users
 * @returns {Array} array of users.
 */
router.get("/", function(req, res, next) {
  if (req && users) {
    res.send(users);
  }
});
/**
 * This function give out concrete user.
 * @param {number} id id of user
 * @returns {Object} data of user.
 */
router.get("/:id", function(req, res, next) {
  if (req.params.id) {
    const idx = users.findIndex(item => item._id === req.params.id);
    res.send(users[idx]);
  } else {
    res.status(404).send("User not found");
  }
});
/**
 * This function take one user and add him to users array.
 * @param {object} user
 * @returns {string} message about act.
 */
router.post("/", function(req, res, next) {
  const lastIdx = +users[users.length - 1]._id + 1;
  users.push({ ...req.body, _id: lastIdx });

  res.send(req.body);
});
/**
 * This function update user by ID.
 * @param {number} ID of user
 * @returns {string} message about act.
 */
router.put("/:id", function(req, res, next) {
  const id = req.params.id;
  if (id) {
    const idx = users.findIndex(item => item._id === id);
    if (idx !== -1) {
      const newFighter = { _id: id, ...users[idx], ...req.body };
      users.splice(idx, 1, newFighter);
      res.send(newFighter);
    } else {
      res.status(404).send("User not found");
    }
  } else {
    res.status(404).send("User not found");
  }
});
/**
 * This function take user by ID and delete him.
 * @param {number} id
 * @returns {array} array of remaining users.
 */
router.delete("/:id", function(req, res, next) {
  if (req.params.id) {
    const idx = users.findIndex(item => item._id === req.params.id);
    if (idx !== -1) {
      users.splice(idx, 1);
      res.send(users);
    } else {
      res.status(404).send("User not found");
    }
  } else {
    res.status(404).send("User not found");
  }
});

module.exports = router;
